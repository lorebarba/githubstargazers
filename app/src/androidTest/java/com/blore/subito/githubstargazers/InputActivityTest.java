package com.blore.subito.githubstargazers;

import android.support.test.espresso.IdlingRegistry;
import android.support.test.espresso.idling.CountingIdlingResource;
import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static android.support.test.espresso.matcher.ViewMatchers.hasErrorText;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.not;

/**
 * Created by lorenzo on 11/25/17.
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class InputActivityTest {

    @Rule
    public ActivityTestRule<InputActivity> mActivityRule = new IntentsTestRule<>(InputActivity.class);
    private CountingIdlingResource stargazersIdlingResource;

    @Before
    public void registerIdlingResource() {
        stargazersIdlingResource = mActivityRule.getActivity().getIdlingResource();
        IdlingRegistry.getInstance().register(stargazersIdlingResource);
    }

    @After
    public void unregisterIdlingResource() {
        IdlingRegistry.getInstance().unregister(stargazersIdlingResource);
    }

    @Test
    public void buttonSearchStargazersDisplayed() {
        onView(withText("VIEW STARGAZERS!")).check(matches(isDisplayed()));
    }

    @Test
    public void knownStargazersFound() throws InterruptedException {
        onView(withId(R.id.et_username)).perform(typeText("octocat"));
        onView(withId(R.id.atv_reponame)).perform(typeText("hello-worId"));
        onView(withId(R.id.btn_search_stargazers)).perform(click());

        intended(hasComponent(StargazerActivity.class.getName()));
    }

    @Test
    public void inexistentStargazersNotFound() throws InterruptedException {
        onView(withId(R.id.et_username)).perform(typeText("octocat"));
        onView(withId(R.id.atv_reponame)).perform(typeText("invented_repo_name"));
        onView(withId(R.id.btn_search_stargazers)).perform(click());

        not(hasComponent(StargazerActivity.class.getName()));
    }

    @Test
    public void inexistentUserNotFound() throws InterruptedException {
        onView(withId(R.id.et_username)).perform(typeText("invented_user_name"));
        onView(withId(R.id.atv_reponame)).perform(typeText("invented_repo_name"));
        onView(withId(R.id.btn_search_stargazers)).perform(click());

        not(hasComponent(StargazerActivity.class.getName()));
    }

    @Test
    public void emptyUserNotFound() throws InterruptedException {
        onView(withId(R.id.et_username)).perform(typeText(""));
        onView(withId(R.id.atv_reponame)).perform(typeText("invented_repo_name"));
        onView(withId(R.id.btn_search_stargazers)).perform(click());

        not(hasComponent(StargazerActivity.class.getName()));
    }

    @Test
    public void emptyUserAndRepoNotFound() throws InterruptedException {
        onView(withId(R.id.et_username)).perform(typeText(""));
        onView(withId(R.id.atv_reponame)).perform(typeText(""));
        onView(withId(R.id.btn_search_stargazers)).perform(click());

        not(hasComponent(StargazerActivity.class.getName()));
    }

    @Test
    public void isErrorDisplayedForNoUserFound() throws InterruptedException {
        onView(withId(R.id.et_username)).perform(typeText("invented_name_4ik83rg"));
        onView(withId(R.id.atv_reponame)).perform(typeText("anything"));
        onView(withId(R.id.btn_search_stargazers)).perform(click());
        onView(withId(R.id.et_username)).check(matches(hasErrorText(mActivityRule.getActivity().getString(R.string.user_not_found))));
    }

    @Test
    public void isErrorDisplayedForNoRepoFound() throws InterruptedException {
        onView(withId(R.id.et_username)).perform(typeText("lorebarba"));
        onView(withId(R.id.atv_reponame)).perform(typeText("invented_repo"));
        onView(withId(R.id.btn_search_stargazers)).perform(click());
        onView(withId(R.id.atv_reponame)).check(matches(hasErrorText(mActivityRule.getActivity().getString(R.string.repo_not_found_for_user))));
    }
}