package com.blore.subito.githubstargazers.stargazers;

import com.blore.subito.githubstargazers.data.model.Stargazer;
import com.blore.subito.githubstargazers.util.mvp.IBasePresenter;

import java.util.List;

/**
 * Created by lorenzo on 11/24/17.
 */

public class StargazerContract {
    public interface View {

        // user and repositories

        void displayRepoNames(String[] reponames);

        void displayUserNotFound(String username);

        void displayNoReposFoundForUser(String username);

        // repository and stargazer

        void displayNoStargazersFound();

        void displayRepoInexistentForUser(String username, String reponame);

        void displayStargazers(List<Stargazer> stargazers);

        // generic errors

        void displayNoInternetConnection();

        void displayRateLimitExceeded();

        void displayOtherError(int code);

    }

    public interface Presenter extends IBasePresenter<View> {

        void loadRepoNames(String username);

        void loadStargazers(String username, String reponame);
    }
}
