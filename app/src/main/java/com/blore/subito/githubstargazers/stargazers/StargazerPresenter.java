package com.blore.subito.githubstargazers.stargazers;

import com.blore.subito.githubstargazers.data.DataSource;
import com.blore.subito.githubstargazers.data.model.Repo;
import com.blore.subito.githubstargazers.data.model.Stargazer;
import com.blore.subito.githubstargazers.data.remote.Errors;
import com.blore.subito.githubstargazers.data.remote.RemoteDataSource;
import com.blore.subito.githubstargazers.util.Util;
import com.blore.subito.githubstargazers.util.mvp.BasePresenter;
import com.blore.subito.githubstargazers.util.threading.MainUiThread;

import java.util.List;

/**
 * Created by lorenzo on 11/24/17.
 */

public class StargazerPresenter extends BasePresenter<StargazerContract.View> implements StargazerContract.Presenter {

    private final MainUiThread handler;

    public StargazerPresenter(StargazerContract.View view) {
        this.view = view;
        handler = MainUiThread.getInstance();
    }

    @Override
    public void loadRepoNames(final String username) {

        DataSource dataSource = RemoteDataSource.getInstance();

        dataSource.getRepos(username, new DataSource.GetReposCallback() {
            @Override
            public void onSuccess(final Repo[] repos) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (view != null) {
                            if (repos.length == 0)
                                view.displayNoReposFoundForUser(username);
                            else
                                view.displayRepoNames(Util.getNamesFromRepos(repos));
                        }
                    }
                });
            }

            @Override
            public void onNetworkFailure(final int code) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        switch (code) {
                            case Errors.NOT_FOUND:
                                view.displayUserNotFound(username);
                                break;
                        }
                    }
                });
            }
        });
    }

    @Override
    public void loadStargazers(final String username, final String reponame) {
        DataSource dataSource = RemoteDataSource.getInstance();

        dataSource.getStargazers(username, reponame, new DataSource.GetStargazersCallback() {
            @Override
            public void onSuccess(final List<Stargazer> stargazers) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (view != null) {
                            if (stargazers.isEmpty())
                                view.displayNoStargazersFound();
                            else
                                view.displayStargazers(stargazers);
                        }
                    }
                });
            }

            @Override
            public void onNetworkFailure(final int code) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        switch (code) {
                            case Errors.NOT_FOUND:
                                view.displayRepoInexistentForUser(username, reponame);
                                break;
                            case Errors.NO_INTERNET:
                                view.displayNoInternetConnection();
                                break;
                            case Errors.FORBIDDEN:
                                view.displayRateLimitExceeded();
                                break;
                            default:
                                view.displayOtherError(code);
                        }
                    }
                });
            }
        });
    }
}
