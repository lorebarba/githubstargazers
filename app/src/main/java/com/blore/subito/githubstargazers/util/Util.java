package com.blore.subito.githubstargazers.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.blore.subito.githubstargazers.data.model.Repo;

/**
 * Created by lorenzo on 11/24/17.
 */

public class Util {

    public static String[] getNamesFromRepos(Repo[] repoList) {
        String[] repoNameArray = new String[repoList.length];
        for (int i = 0; i < repoList.length; i++) {
            repoNameArray[i] = repoList[i].getName();
        }
        return repoNameArray;
    }


    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }
}
