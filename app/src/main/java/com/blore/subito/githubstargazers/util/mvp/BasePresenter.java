package com.blore.subito.githubstargazers.util.mvp;

/**
 * Created by lorenzo on 11/24/17.
 */

public class BasePresenter<ViewType> implements IBasePresenter<ViewType> {

    protected ViewType view;

    @Override
    public void onViewActive(ViewType view) {
        this.view = view;
    }

    @Override
    public void onViewInactive() {
        view = null;
    }
}
