
package com.blore.subito.githubstargazers.data.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Stargazer implements Parcelable {
    private String login;
    private String avatar_url;

    public String getLogin() {
        return login;
    }

    public String getAvatar_url() {
        return avatar_url;
    }


    // code for serialization

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.login);
        dest.writeString(this.avatar_url);
    }

    public Stargazer() {
    }

    protected Stargazer(Parcel in) {
        this.login = in.readString();
        this.avatar_url = in.readString();
    }

    public static final Creator<Stargazer> CREATOR = new Creator<Stargazer>() {
        @Override
        public Stargazer createFromParcel(Parcel source) {
            return new Stargazer(source);
        }

        @Override
        public Stargazer[] newArray(int size) {
            return new Stargazer[size];
        }
    };
}