package com.blore.subito.githubstargazers;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.VisibleForTesting;
import android.support.test.espresso.idling.CountingIdlingResource;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.blore.subito.githubstargazers.data.model.Stargazer;
import com.blore.subito.githubstargazers.data.remote.RemoteDataSource;
import com.blore.subito.githubstargazers.util.mvp.BaseView;
import com.blore.subito.githubstargazers.stargazers.StargazerContract;
import com.blore.subito.githubstargazers.stargazers.StargazerPresenter;
import com.blore.subito.githubstargazers.util.Util;

import java.util.ArrayList;
import java.util.List;

import static com.blore.subito.githubstargazers.StargazerActivity.STARGAZERS;

public class InputActivity extends BaseView implements StargazerContract.View {
    private StargazerContract.Presenter presenter;
    private AutoCompleteTextView atvReponame;
    private EditText etUsername;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input);
        presenter = new StargazerPresenter(this);
        progressBar = (ProgressBar) findViewById(R.id.progress_stargazers);

        etUsername = (EditText) findViewById(R.id.et_username);
        atvReponame = (AutoCompleteTextView) findViewById(R.id.atv_reponame);
        atvReponame.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                String username = getTextFromInputField(R.id.et_username);
                if (hasFocus && !username.isEmpty()) {
                    if (Util.isNetworkAvailable(getApplicationContext()))
                        presenter.loadRepoNames(username);
                }
            }
        });
    }

    public void onClickStargazers(View view) {
        closeKeyboard(view);
        String username = getTextFromInputField(R.id.et_username);
        String reponame = getTextFromInputField(R.id.atv_reponame);

        if (!username.isEmpty() && !reponame.isEmpty()) {
            if (Util.isNetworkAvailable(getApplicationContext())) {
                setProgressBar(true);
                presenter.loadStargazers(username, reponame);
            } else
                displayNoInternetConnection();
        }
    }


    // from contract

    @Override
    public void displayRepoNames(String[] reponames) {
        setProgressBar(false);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, reponames);
        atvReponame.setAdapter(adapter);
    }

    @Override
    public void displayUserNotFound(String username) {
        setProgressBar(false);
        etUsername.setError(getString(R.string.user_not_found));
    }

    @Override
    public void displayNoReposFoundForUser(String username) {
        setProgressBar(false);
        etUsername.setError(getString(R.string.user_has_no_repo));
        atvReponame.setAdapter(null);
    }

    @Override
    public void displayNoStargazersFound() {
        setProgressBar(false);
        atvReponame.setError(getString(R.string.no_stargazers_found));
        requestFocus(atvReponame);
    }

    @Override
    public void displayRepoInexistentForUser(String username, String reponame) {
        setProgressBar(false);
        atvReponame.setError(getString(R.string.repo_not_found_for_user));
    }

    @Override
    public void displayStargazers(List<Stargazer> stargazers) {
        setProgressBar(false);
        Intent intent = new Intent(InputActivity.this, StargazerActivity.class);
        intent.putParcelableArrayListExtra(STARGAZERS, new ArrayList<Parcelable>(stargazers));
        startActivity(intent);
    }


    @Override
    public void displayNoInternetConnection() {
        setProgressBar(false);
        notifyError(getString(R.string.no_internet));
    }

    @Override
    public void displayRateLimitExceeded() {
        setProgressBar(false);
        notifyError(getString(R.string.rate_exceeded));
    }

    @Override
    public void displayOtherError(int code) {
        setProgressBar(false);
        notifyError("Other error (" + code + ")");
    }

    // other

    @Override
    public Context getContext() {
        return this;
    }


    @VisibleForTesting
    public CountingIdlingResource getIdlingResource() {
        return RemoteDataSource.getInstance().getStargazersIdlingResource();
    }
}
