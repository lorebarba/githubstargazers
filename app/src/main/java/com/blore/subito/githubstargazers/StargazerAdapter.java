package com.blore.subito.githubstargazers;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.blore.subito.githubstargazers.data.model.Stargazer;
import com.blore.subito.githubstargazers.util.CircleTransform;
import com.squareup.picasso.Picasso;

import java.util.List;


/**
 * Created by lorenzo on 11/24/17.
 */

public class StargazerAdapter extends RecyclerView.Adapter<StargazerAdapter.ViewHolder> {
    private List<Stargazer> mStargazerList;
    private Context ctx;


    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvStargazerUsername;
        ImageView ivStargazerAvatar;

        ViewHolder(View v) {
            super(v);
            tvStargazerUsername = (TextView) v.findViewById(R.id.tv_stargazer_username);
            ivStargazerAvatar = (ImageView) v.findViewById(R.id.iv_stargazer_avatar);
        }
    }

    public StargazerAdapter(List<Stargazer> stargazerList, Context ctx) {
        mStargazerList = stargazerList;
        this.ctx = ctx;
    }

    @Override
    public StargazerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_row, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Stargazer oneStargazer = mStargazerList.get(position);
        holder.tvStargazerUsername.setText(oneStargazer.getLogin());

        Picasso.with(ctx).load(oneStargazer.getAvatar_url()).transform(new CircleTransform())
                .into(holder.ivStargazerAvatar);
    }

    @Override
    public int getItemCount() {
        return mStargazerList.size();
    }
}