package com.blore.subito.githubstargazers.data.remote;

import android.support.annotation.NonNull;
import android.support.test.espresso.idling.CountingIdlingResource;
import android.text.TextUtils;
import android.util.Log;

import com.blore.subito.githubstargazers.data.DataSource;
import com.blore.subito.githubstargazers.data.model.Repo;
import com.blore.subito.githubstargazers.data.model.Stargazer;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by lorenzo on 11/24/17.
 */

public class RemoteDataSource extends DataSource {
    private static final String BASE_API_URL = "https://api.github.com/";
    private static final String PATH_USERS = "users";
    private static final String PATH_REPOS = "repos";
    private static final String PATH_STARGAZERS = "stargazers";
    private static final String TAG = RemoteDataSource.class.getSimpleName();
    private OkHttpClient client;

    private static RemoteDataSource instance;


    public static synchronized RemoteDataSource getInstance() {
        if (instance == null)
            instance = new RemoteDataSource();
        return instance;
    }


    private RemoteDataSource() {
        stargazersIdlingResource = new CountingIdlingResource("httpIdlingRes");
    }

    @Override
    public void getStargazers(String username, String reponame, final GetStargazersCallback stargazersCallback) {
        String url = compoundUrl(PATH_REPOS, username, reponame, PATH_STARGAZERS);

        stargazersIdlingResource.increment();

        Callback filterCallback = new FilterCallback(stargazersCallback) {
            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                super.onFailure(call, e);
                stargazersIdlingResource.decrement();
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                super.onResponse(call, response);
                stargazersIdlingResource.decrement();
                if (errorOccourred) return;

                try {
                    Stargazer[] repoList = new Gson().fromJson(responseBodyString, Stargazer[].class);
                    stargazersCallback.onSuccess(repoList == null ? new ArrayList<Stargazer>() : Arrays.asList(repoList));
                } catch (JsonSyntaxException jse) {
                    stargazersCallback.onNetworkFailure(Errors.OTHER_ERROR);
                }
            }
        };
        GET(url, filterCallback);
    }

    @Override
    public void getRepos(String username, final GetReposCallback repoCallback) {
        String url = compoundUrl(PATH_USERS, username, PATH_REPOS);

        Callback filterCallback = new FilterCallback(repoCallback) {
            @Override
            public void onResponse(@NonNull Call call, @NonNull okhttp3.Response response) throws IOException {
                super.onResponse(call, response);
                if (errorOccourred) return;

                try {
                    Repo[] repoList = new Gson().fromJson(responseBodyString, Repo[].class);
                    repoCallback.onSuccess(repoList);
                } catch (JsonSyntaxException jse) {
                    repoCallback.onNetworkFailure(Errors.OTHER_ERROR);
                }
            }
        };
        GET(url, filterCallback);
    }


    private void GET(String url, Callback callback) {
        Request request = new Request.Builder()
                .url(url)
                .build();
        Log.d(TAG, "get url: " + url);
        getHttpClient().newCall(request).enqueue(callback);
    }


    private String compoundUrl(String... urlParts) {
        return BASE_API_URL + TextUtils.join("/", urlParts);
    }


    private OkHttpClient getHttpClient() {
        if (client == null) {
            client = new OkHttpClient();
            Log.wtf(TAG, "REQUESTED HTTP CLIENT CREATION");
        }
        return client;
    }
}
