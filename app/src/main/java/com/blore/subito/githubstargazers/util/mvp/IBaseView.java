package com.blore.subito.githubstargazers.util.mvp;

import android.content.Context;

/**
 * Created by lorenzo on 11/24/17.
 */


public interface IBaseView {

    void showToastMessage(String message);

    void setProgressBar(boolean show);

    Context getContext();
}
