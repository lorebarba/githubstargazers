package com.blore.subito.githubstargazers.data;

import android.support.test.espresso.idling.CountingIdlingResource;

import com.blore.subito.githubstargazers.data.model.Repo;
import com.blore.subito.githubstargazers.data.model.Stargazer;

import java.util.List;

/**
 * Created by lorenzo on 11/24/17.
 */


public abstract class DataSource {

    protected CountingIdlingResource stargazersIdlingResource;


    public CountingIdlingResource getStargazersIdlingResource() {
        return stargazersIdlingResource;
    }


    public interface NetworkListener {
        void onNetworkFailure(int code);
    }

    public interface GetStargazersCallback extends NetworkListener {
        void onSuccess(List<Stargazer> stargazers);
    }

    public interface GetReposCallback extends NetworkListener {
        void onSuccess(Repo[] repos);
    }

    public abstract void getRepos(String username, GetReposCallback callback);

    public abstract void getStargazers(String username, String reponame, GetStargazersCallback callback);
}
