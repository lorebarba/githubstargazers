package com.blore.subito.githubstargazers.data.remote;

/**
 * Created by lorenzo on 11/24/17.
 */

public class Errors {
    public static final int NO_INTERNET = 0;
    public static final int OTHER_ERROR = 1;
    public static final int FORBIDDEN = 403;
    public static final int NOT_FOUND = 404;
}
