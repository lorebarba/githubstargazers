package com.blore.subito.githubstargazers.util.mvp;

/**
 * Created by lorenzo on 11/24/17.
 */

public interface IBasePresenter<ViewType> {
    void onViewActive(ViewType view);

    void onViewInactive();
}
