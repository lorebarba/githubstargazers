package com.blore.subito.githubstargazers;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.blore.subito.githubstargazers.data.model.Stargazer;
import com.blore.subito.githubstargazers.util.mvp.BaseView;

import java.util.List;

/**
 * Created by lorenzo on 11/24/17.
 */

public class StargazerActivity extends BaseView {

    public static final String STARGAZERS = "stargazers";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stargazers);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // So dummy that no mvp in this activity

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.rv_stargazers);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        List<Stargazer> stargazers = getIntent().getParcelableArrayListExtra(STARGAZERS);

        recyclerView.setAdapter(new StargazerAdapter(stargazers, this));
    }

    @Override
    public Context getContext() {
        return this;
    }
}