package com.blore.subito.githubstargazers.data.remote;

import android.support.annotation.NonNull;
import android.util.Log;

import com.blore.subito.githubstargazers.data.DataSource;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;
import okhttp3.ResponseBody;

/**
 * Created by lorenzo on 11/24/17.
 */

class FilterCallback implements Callback {
    private static final String TAG = FilterCallback.class.getSimpleName();
    private DataSource.NetworkListener networkListener;
    String responseBodyString;
    boolean errorOccourred = false;

    private void error(int code) {
        errorOccourred = true;
        networkListener.onNetworkFailure(code);
    }

    FilterCallback(DataSource.NetworkListener networkListener) {
        this.networkListener = networkListener;
    }

    @Override
    public void onFailure(@NonNull Call call, @NonNull IOException e) {
        error(Errors.NO_INTERNET);

    }

    @Override
    public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
        try (ResponseBody responseBody = response.body()) {
            if (!response.isSuccessful()) {
                error(response.code());
                return;
            }
            if (responseBody == null) {
                error(Errors.OTHER_ERROR);
                return;
            }

            responseBodyString = responseBody.string();
            Log.d(TAG, "response: " + responseBodyString);
        }
    }
}